"""
URLs for Matrix Project

Copyright (C) 2015

Distributed under FreeBSD Licence

Authors:
    Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
    Pamela Milagros Garcia Vasquez <pamela.garcia.vs@gmail.com>
"""
from django.conf.urls import include, url

urlpatterns = [
    url(r'^', include('apps.matrix.urls')),
]
