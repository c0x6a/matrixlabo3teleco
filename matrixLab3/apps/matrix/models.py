"""
Models for Matrix app

Copyright (C) 2015

Distributed under FreeBSD Licence

Authors:
    Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
    Pamela Milagros Garcia Vasquez <pamela.garcia.vs@gmail.com>
"""
from django.db import models


class Matrix(models.Model):
    """
    Matrix Model
    """
    row = models.IntegerField()
    column = models.IntegerField()
    value = models.IntegerField()
    current_value = models.BooleanField(default=True)
    last_modified = models.BooleanField(default=False)

    def __str__(self):
        return str(self.value)

    # def save(self, **kwargs):
    #     self.last_modified = True
    #     super(Matrix, self).save()

    def to_8bit_binary(self):
        """
        Returns value converted to 8bit binary

        :return: 8bit value
        :rtype: String
        """
        return '{0:08b}'.format(self.value)

    def past_values(self):
        """
        Returns all past values of the current cell

        :return: Past values
        :rtype: List
        """
        try:
            values_list = []
            values = Matrix.objects.filter(row=self.row, column=self.column,
                                           current_value=False)
            for value in values:
                values_list.append(value.value)
        except Exception:
            values_list = []

        return values_list
