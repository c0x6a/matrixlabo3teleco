"""
URLs for Matrix app

Copyright (C) 2015

Distributed under FreeBSD Licence

Authors:
    Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
    Pamela Milagros Garcia Vasquez <pamela.garcia.vs@gmail.com>
"""
from django.conf.urls import url

from apps.matrix.views import index, get_value_data

urlpatterns = [
    url(r'^$', index),
    url(r'^(?P<row>\d+)/(?P<column>\d+)/$', get_value_data),
]
