# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matrix', '0002_auto_20150804_0450'),
    ]

    operations = [
        migrations.AddField(
            model_name='matrix',
            name='last_modified',
            field=models.BooleanField(default=False),
        ),
    ]
