# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matrix', '0003_matrix_last_modified'),
    ]

    operations = [
        migrations.RenameField(
            model_name='matrix',
            old_name='i',
            new_name='column',
        ),
        migrations.RenameField(
            model_name='matrix',
            old_name='j',
            new_name='row',
        ),
    ]
