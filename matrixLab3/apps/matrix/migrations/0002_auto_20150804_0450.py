# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matrix', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='matrix',
            name='last_value',
        ),
        migrations.AddField(
            model_name='matrix',
            name='current_value',
            field=models.BooleanField(default=True),
        ),
    ]
