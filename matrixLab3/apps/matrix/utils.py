"""
Utils for Matrix app

Copyright (C) 2015

Distributed under FreeBSD Licence

Authors:
    Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
    Pamela Milagros Garcia Vasquez <pamela.garcia.vs@gmail.com>
"""
import json
import random

from django.http import HttpResponse


def generate_matrix(Matrix):
    """
    Generates a 25x8 random numbers matrix

    :return: Matrix
    :rtype:  List
    """
    matrix = []

    for i in range(0, 8):
        row = []
        for j in range(0, 25):
            row_element = Matrix.objects.get_or_create(
                row=i, column=j, value=random.randint(0, 255))[0]
            row.append(row_element)

        matrix.append(row)

    return matrix


def get_matrix_from_db(Matrix):
    """
    Gets matrix values from database

    :param Matrix: Matrix model
    :return: Matrix
    :rtype:  List
    """
    matrix = []
    for i in range(0, 8):
        matrix.append(
            [Matrix.objects.get(row=i, column=j, current_value=True) for j in
             range(0, 25)])

    return matrix


def json_response(response_dict, status=200):
    """
    Returns a JSON dumped from a dict

    :param response_dict: dictionary to convert to json
    :param status: Response status
    :return: dictionary data as JSON
    :rtype: JSON
    """
    response = HttpResponse(
        json.dumps(response_dict),
        content_type="application/json", status=status)
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    return response


def discard_last_modified(Matrix):
    """
    Discards last_modified status from the last modified cell on matrix

    :param Matrix: Matrix model
    """
    try:
        last_modified_cell = Matrix.objects.get(last_modified=True)
        last_modified_cell.last_modified = False
        last_modified_cell.save()
    except Exception as e:
        print(e.args)


def discard_current_value(Matrix, column, row):
    """
    Discards current value stored on cell

    :param Matrix: Matrix model
    :param column: column index
    :param row:    row index
    """
    current_cell = Matrix.objects.get(
        row=row, column=column, current_value=True)
    current_cell.current_value = False
    current_cell.save()
