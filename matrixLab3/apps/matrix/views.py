"""
Views for Matrix app

Copyright (C) 2015

Distributed under FreeBSD Licence

Authors:
    Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
    Pamela Milagros Garcia Vasquez <pamela.garcia.vs@gmail.com>
"""
from django.shortcuts import render

from apps.matrix.models import Matrix
from apps.matrix.utils import generate_matrix, get_matrix_from_db
from apps.matrix.utils import discard_last_modified, discard_current_value
from apps.matrix.utils import json_response


def index(request):
    """
    Index view

    :param request: Web request
    :return: Index web page
    :rtype: HttpResponse
    """
    errors = None

    if request.method == "POST":
        row = request.POST.get('row', '')
        column = request.POST.get('column', '')
        value = request.POST.get('value', '')

        discard_current_value(Matrix, column, row)
        discard_last_modified(Matrix)

        new_cell, is_new = Matrix.objects.get_or_create(row=row, column=column,
                                                        value=value)
        new_cell.last_modified = True
        new_cell.save()

    matrix_len = Matrix.objects.all().__len__()

    if matrix_len == 0:
        matrix = generate_matrix(Matrix)
    else:
        matrix = get_matrix_from_db(Matrix)

    data = {
        'matrix': matrix,
        'errors': errors
    }
    return render(request, 'index.html', data)


def get_value_data(request, row, column):
    """
    Gets current cell data

    :param request: Web request
    :param row:  row index
    :param column: column index
    :return: Current cell data
    :rtype: JSON
    """
    cell_data = Matrix.objects.get(row=row, column=column, current_value=True)
    data = {
        'row': cell_data.row,
        'column': cell_data.column,
        'value': cell_data.value,
        'binary_8bit': cell_data.to_8bit_binary(),
        'past_values': cell_data.past_values()
    }
    return json_response(data)
